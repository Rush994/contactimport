package de.leadfrog.myapplication

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import androidx.activity.result.contract.ActivityResultContract

class ApiPickerActivityContract : ActivityResultContract<Unit, String?>() {

    override fun createIntent(context: Context, input: Unit?): Intent =
        Intent(
            Intent.ACTION_PICK,
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        )

    override fun parseResult(resultCode: Int, intent: Intent?): String? = when
    {
        resultCode != Activity.RESULT_OK -> null
        else -> intent?.getStringExtra("data")
    }
}