package de.leadfrog.myapplication

import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.activity.invoke
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG: String = MainActivity::class.java.simpleName
    private var contactID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnPickContact.setOnClickListener {
            askLocationPermission(android.Manifest.permission.READ_CONTACTS)
        }
    }

    private val askLocationPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
        if(result){
            Log.e("TAG", "permnission granted")
            pickContact.invoke()
        }else{
            Log.e("TAG", "No permnission")
        }
    }

    private val pickContact = registerForActivityResult(ActivityResultContracts.PickContact()) {
        try {
            val contactNumber = retrieveContactNumber(it)
            val contactName = retrieveContactName(it)
            // getting contacts ID
            val cursorID = contentResolver.query(it, arrayOf(ContactsContract.Contacts._ID),
                null, null, null)
            if (cursorID!!.moveToFirst()) {
                contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID))
            }
            val rawContactId: String = getRawContactId(contactID!!)
            val contactCompany = getCompanyName(rawContactId)
            val contactEmail = retrieveContactEmail(it)
            tvContactName.text = getString(R.string.contact_name, contactName)
            tvContactNumber.text = getString(R.string.contact_number, contactNumber)
            tvContactEmail.text = getString(R.string.contact_email, contactEmail)
            tvContactCompany.text = getString(R.string.contact_company, contactCompany)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun retrieveContactNumber(uriContact: Uri): String {
        var contactNumber = ""

        // getting contacts ID
        val cursorID = contentResolver.query(uriContact, arrayOf(ContactsContract.Contacts._ID),
                null, null, null)
        if (cursorID!!.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID))
        }
        cursorID.close()
        Log.d(TAG, "Contact ID: $contactID")

        // Using the contact ID now we will get contact phone number
        val cursorPhone = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER),
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE, arrayOf(contactID),
                null)
        if (cursorPhone!!.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
        }
        cursorPhone.close()
        Log.d(TAG, "Contact Phone Number: $contactNumber")
        return contactNumber
    }

    private fun retrieveContactName(uriContact: Uri): String {
        var contactName = ""
        val cursor = contentResolver.query(uriContact, null, null, null, null);
        if (cursor!!.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }
        cursor.close();
        Log.d(TAG, "Contact Name: $contactName")
        return contactName
    }

    private fun retrieveContactEmail(uriContact: Uri): String {
        var contactEmail = ""

        // getting contacts ID
        val cursorID = contentResolver.query(uriContact, arrayOf(ContactsContract.Contacts._ID),
            null, null, null)
        if (cursorID!!.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID))
        }
        cursorID.close()
        Log.d(TAG, "Contact ID: $contactID")

        // Using the contact ID now we will get contact phone number
        val cursorPhone = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, arrayOf(ContactsContract.CommonDataKinds.Email.ADDRESS),
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(contactID),
            null)
        if (cursorPhone!!.moveToFirst()) {
            contactEmail = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
        }
        cursorPhone.close()
        Log.d(TAG, "Contact Email: $contactEmail")
        return contactEmail
    }

    private fun getRawContactId(contactId: String): String {
        val projection =
            arrayOf(ContactsContract.RawContacts._ID)
        val selection = ContactsContract.RawContacts.CONTACT_ID + "=?"
        val selectionArgs = arrayOf(contactId)
        val c = contentResolver.query(
            ContactsContract.RawContacts.CONTENT_URI,
            projection,
            selection,
            selectionArgs,
            null
        )
            ?: return ""
        var rawContactId = -1
        if (c.moveToFirst()) {
            rawContactId = c.getInt(c.getColumnIndex(ContactsContract.RawContacts._ID))
        }
        c.close()
        return rawContactId.toString()
    }

    private fun getCompanyName(rawContactId: String): String? {
        return try {
            val orgWhere =
                ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?"
            val orgWhereParams = arrayOf(
                rawContactId,
                ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
            )
            val cursor = contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                null,
                orgWhere,
                orgWhereParams,
                null
            )
                ?: return null
            var name: String? = null
            if (cursor.moveToFirst()) {
                name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY))
            }
            cursor.close()
            name
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            null
        }
    }
}
